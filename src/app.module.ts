import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service';
import { AuthModule } from './auth/auth.module';
 
@Module({
  imports: [
    TypeOrmModule.forRoot({
        type: "postgres",
        host: "localhost",
        port: 5432,
        username: "postgres",
        password: "",
        database: "systempx_pr_dev",
        logging: true,
        dropSchema: false,
        entities: ["dist/**/*.entity.js"],
        synchronize: false,
        migrationsRun: true,
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}