module.exports = {
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "",
  database: "systempx_pr_dev",
  logging: true,
  dropSchema: false,
  entities: ["dist/**/*.entity.js"],
  synchronize: false,
  migrationsRun: true,
  cli: {
    entitiesDir: ["src/**/entities"],
    migrationsDir: ["src/migrations"],
    subscribersDir: ["src/subscribers"],
  }
}